import React from 'react';
import CardItem from "../../components/CardItem/CardItem";
import PropTypes from "prop-types";
import {connect} from "react-redux";

const Favorites = ({cardsFavorites, page, deleteFavorites}) => {

    const cards = cardsFavorites.map(el => <CardItem
                              card={el}
                              key={el.article}
                              page={page}
                              deleteFavorites={deleteFavorites}
    />);
    return (
        <div className="ListItems">
            {cards}
        </div>
    );
};
Favorites.propTypes = {
    deleteFavorites: PropTypes.func,
    cardsFavorites: PropTypes.array.isRequired,
    page: PropTypes.string.isRequired
};

const mapStoreToProps = ({cardsFavorites}) => {
    return {
        cardsFavorites
    }
};
export default connect(mapStoreToProps)(Favorites);