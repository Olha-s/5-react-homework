const initialStore = {
    items: [],
    modalShow: false,
    modalShowCart: false,
    cardsCart: [],
    cardsFavorites: [],
    cartFormValue: []
};
export default initialStore;