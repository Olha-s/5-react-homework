import React from 'react';
import './Modal.scss';
import cross from '../../images/cross.png';
import PropTypes from "prop-types";

const Modal = ({onClick, header, text, actions}) => {

    const wrapperClose = (e) => {
        const atrClass = e.target.getAttribute('class');
        if (atrClass === "ModalWrapper" || atrClass === "Modal__btn" || atrClass === "Modal__cross") {
            onClick();
           }
    };
    return (
        <div className="ModalWrapper" onClick={wrapperClose}>
            <div className="Modal">
                <div className="Modal__header">
                    <p className="Modal__header-text">{header}</p>
                    <span><img src={cross} alt="" className="Modal__cross"/></span>
                </div>
                <div>
                    <p className="Modal__main">{text}</p>
                    <div className="Modal__buttons">{actions}</div>
                </div>
            </div>
        </div>
    );
};


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Modal;