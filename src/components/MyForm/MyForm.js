import React from 'react';
import {Form, Field, Formik, ErrorMessage} from 'formik';
import * as yup from 'yup';
import {connect} from "react-redux";
import {deleteAllCardsCartAction, getInfoFormAction} from "../../store/actions/actions";
import PropTypes from "prop-types";
import './MyForm.scss'

const formSchema = yup.object().shape({
    name: yup
        .string()
        .required("This field is required")
        .min(2, "Minimum length is 2 symbols"),
    surname: yup
        .string()
        .required("This field is required")
        .min(2, "Minimum length is 2 symbols"),
    age: yup
        .number()
        .required("This field is required")
        .positive("Value must be a positive number")
        .integer("Validates that a number is an integer")
        // .number.moreThan(17, "Age must be over 18")
        .min(2, "Minimum length is 2 symbols"),
    address: yup
        .string()
        .required("This field is required")
        .min(5, "Minimum length is 5 symbols"),
    tel: yup
        .number()
        .required("This field is required")
        .positive("Value must be a positive number")
        .integer("Validates that a number is an integer")
        .min(10, "Minimum length is 10 symbols"),
});

const MyForm = ({getInfoForm, cardsCart, deleteAllCardsCart}) => {
    const handleSubmit = (value, {setSubmitting}) => {
        setSubmitting(false);
        getInfoForm(value, cardsCart);
        deleteAllCardsCart();
    };

    return (
        <Formik
            initialValues={{
                name: '',
                surname: '',
                age: '',
                address: '',
                tel: ''
            }}
            validationSchema={formSchema}
            onSubmit={handleSubmit}
        >
            {(formikProps) => {

                return (
                    <Form onSubmit={formikProps.handleSubmit} className="Form" noValidate>
                        <div className="Form__input-wrapper">
                            <label>
                                <Field component='input' type='text' name='name' placeholder='Name'
                                       className="Form__input"/>
                                <span className="Form__msg"><ErrorMessage name='name'/></span></label>
                            <label>
                                <Field component='input' type='text' name='surname' placeholder='Surname'
                                       className="Form__input"/>
                                <span className="Form__msg"><ErrorMessage name='surname'/></span></label>
                            <label>
                                <Field component='input' type='text' name='age' placeholder='Age'
                                       className="Form__input"/>
                                <span className="Form__msg"><ErrorMessage name='age'/></span></label>
                            <label>
                                <Field component='input' type='text' name='address' placeholder='Delivery address'
                                       className="Form__input"/>
                                <span className="Form__msg"><ErrorMessage name='address'/></span></label>
                            <label>
                                <Field component='input' type='tel' name='tel' placeholder='phone number'
                                       className="Form__input"/>
                                <span className="Form__msg"><ErrorMessage name='tel'/></span></label>
                        </div>
                        <div>
                            <button className="Form__btn" type='submit' disabled={formikProps.submitting}>Checkout
                            </button>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    );
};
MyForm.propTypes = {
    getInfoForm: PropTypes.func.isRequired,
    cardsCart: PropTypes.array.isRequired,
    deleteAllCardsCart: PropTypes.func.isRequired
};

const mapStoreToProps = ({cardsCart}) => {
    return {
        cardsCart
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getInfoForm: (valueForm, cardsCart) => dispatch(getInfoFormAction(valueForm, cardsCart)),
        deleteAllCardsCart: () => dispatch(deleteAllCardsCartAction()),
    }
};
export default connect(mapStoreToProps, mapDispatchToProps)(MyForm);
