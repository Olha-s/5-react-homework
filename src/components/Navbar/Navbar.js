import React from 'react';
import './Navbar.scss';
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <div>
            <ul className="navbar__wrapper">
                <li className="navbar__wrapper-link">
                    <NavLink to="/main" activeClassName='link-active' className='navbar__link'>Main</NavLink>
                </li>
                <li className="navbar__wrapper-link">
                    <NavLink to="/favorites" activeClassName='link-active' className='navbar__link'>Favorites</NavLink>
                </li>
                <li className="navbar__wrapper-link">
                    <NavLink to="/cart" activeClassName='link-active' className='navbar__link'>Cart</NavLink>
                </li>
            </ul>
        </div>
    );
};

export default Navbar;